﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BonusProgram
{
    public class PurchaseBonus : Bonus
    {

        private const int minBonusSum = 5;
        private decimal bonusMoney;
        private decimal price;

        public PurchaseBonus(decimal bonusMoney, decimal price)
        {
            this.bonusMoney = bonusMoney;
            this.price = price;
        }

        public override decimal GetBonus()
        {
            if (price > minBonusSum)
            {
                bonusMoney += price / 100;
            }

            return bonusMoney;
        }

    }
}

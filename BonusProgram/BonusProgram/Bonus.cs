﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BonusProgram
{
    public abstract class Bonus
    {
        public abstract decimal GetBonus();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BonusProgram
{
    class Account
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public decimal Money { get; set; } = 1000m;

        public string Currency { get; set; } = "EUR";

        public decimal BonusMoney { get; set; } = 0m;

        public decimal SpentMoney { get; set; } = 0m;

        private int purchase = 0;



        public void changeCurrency(decimal rate, string currecny)
        {
            Currency = currecny;
            Money *= rate;
            BonusMoney *= rate;

        }

        public void buyProduct(decimal price)
        {
            var bonus = new PurchaseBonus(BonusMoney, price);
            purchase++;
            SpentMoney += price;
            Money -= price;

            BonusMoney = bonus.GetBonus();

        }

        public void endTheDay()
        {
            var bonus = new EndTheDayBonus(purchase, BonusMoney, SpentMoney);

            BonusMoney = bonus.GetBonus();
        }

    }
}

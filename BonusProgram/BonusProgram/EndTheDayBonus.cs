﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BonusProgram
{
    public class EndTheDayBonus : Bonus
    {

        private int purchase;
        private const int minPurchases = 10;
        private decimal spentMoney;
        private decimal bonusMoney;

        public EndTheDayBonus(int purchase, decimal bonusMoney, decimal spentMoney) 
        {
            this.purchase = purchase;
            this.spentMoney = spentMoney;
            this.bonusMoney = bonusMoney;
        }

        public override decimal GetBonus()
        {
            if (purchase >= minPurchases)
            {
                bonusMoney += spentMoney / 100;
            }
            return bonusMoney;
        }
    }
}

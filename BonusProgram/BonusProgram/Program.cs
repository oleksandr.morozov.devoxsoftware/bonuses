﻿using System;

namespace BonusProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            var isWorking = true;
            int action;
            decimal price;
            string currency;
            var acc = new Account();

            Console.WriteLine("Input your username: ");
            acc.Username = Console.ReadLine();
            Console.WriteLine("Input your password: ");
            acc.Password = Console.ReadLine();
            Console.WriteLine($"Hi, {acc.Username}");

            while (isWorking)
            {
                Console.WriteLine("0.Exit \t 1. Check your money and bonuses \t 2. Change your currency \t 3. Buy product \t 4. Go to next day");
                Console.Write("Chose your action:");
                action = int.Parse(Console.ReadLine());
                switch (action)
                {
                    case (1):
                        Console.WriteLine($"You have {acc.Money} {acc.Currency} and bonus: {acc.BonusMoney} {acc.Currency} ");
                        break;

                    case (2):
                        Console.Write("Input rate:");
                        price = decimal.Parse(Console.ReadLine());
                        Console.Write("Input currency:");
                        currency = Console.ReadLine();
                        acc.changeCurrency(price, currency);
                        break;

                    case (3):
                        Console.Write("What is the price:");
                        price = decimal.Parse(Console.ReadLine());
                        acc.buyProduct(price);
                        break;

                    case (4):
                        Console.WriteLine("Next day");
                        acc.endTheDay();
                        break;

                    case (0):
                        isWorking = false;
                        break;
                }
            }
        }
    }
}

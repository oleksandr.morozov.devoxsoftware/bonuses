using Shouldly;
using System;
using Xunit;

namespace BonusProgram.Tests
{
    public class EndTheDayBonusTests
    {
        [Fact]
        public void GetBonus_Positive()
        {
            var bonus = new EndTheDayBonus(10,0m,100m);

            var sut = bonus.GetBonus();

            sut.ShouldBe(1m);
        }

        [Fact]
        public void GetBonus_Positive_Condition()
        {
            var bonus = new EndTheDayBonus(5, 0m, 1000m);

            var sut = bonus.GetBonus();

            sut.ShouldBe(0m);
        }
    }
}

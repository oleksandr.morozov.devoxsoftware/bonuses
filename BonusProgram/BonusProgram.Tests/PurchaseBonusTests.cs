using Shouldly;
using System;
using Xunit;

namespace BonusProgram.Tests
{
    public class PurchaseBonusTests
    {
        [Fact]
        public void GetBonus_Positive()
        {
            var bonus = new PurchaseBonus(0m,100m);

            var sut = bonus.GetBonus();

            sut.ShouldBe(1m);
        }

        [Fact]
        public void GetBonus_Positive_Condition()
        {
            var bonus = new PurchaseBonus(0m, 1m);

            var sut = bonus.GetBonus();

            sut.ShouldBe(0m);
        }
    }
}
